class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]

  def index
    @invoices = Invoice.all.where(customer_id: params[:customer_id])
  end

  def all_invoices
    @all_invoices = Invoice.all
  end

  def show
  end

  def new
    @invoice = Invoice.new
  end

  def edit
  end

  def create
    @invoice = Invoice.new(invoice_params)
    @invoice.customer_id = params[:customer_id]

    respond_to do |format|
      if @invoice.save
        update_estimate(@invoice) if @invoice.estimate_id
        format.html { redirect_to "/customers/#{@invoice.customer_id}/invoices/#{@invoice.id}", notice: 'Invoice was successfully created.' }
        format.json { render action: 'show', status: :created, location: @invoice }
      else
        format.html { render action: 'new' }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to "/customers/#{@invoice.customer_id}/invoices/#{@invoice.id}", notice: 'Invoice was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to"/customers/#{@invoice.customer_id}", notice: 'Invoice has been removed.'  }
      format.json { head :no_content }
    end
  end

  private

    def update_estimate(invoice)
      estimate = Estimate.find(invoice.estimate_id)
      estimate.update_column(:invoice_id, invoice.id)
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:invoice_desc_id, :po_number, :invoice_details, :invoice_created_on, :customer_id, :estimate_id)
    end
end
