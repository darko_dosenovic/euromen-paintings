class EstimatesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_estimate, only: [:show, :edit, :update, :destroy]

  def index
    @estimates = Estimate.all.where(customer_id: params[:customer_id])
  end

  def all_estimates
    @all_estimates = Estimate.all
  end

  def show
  end

  def new
    @estimate = Estimate.new
  end

  def edit
  end

  def create
    @estimate = Estimate.new(estimate_params)

    @estimate.customer_id = params[:customer_id]
    @estimate.invoice_id = params[:invoice_id]
    @estimate.estimate_desc_id = Date.today.strftime("%y%m%d").to_s << params[:id].to_s

    respond_to do |format|
      if @estimate.save
            binding.pry
        format.html { redirect_to "/customers/#{@estimate.customer_id}/estimates/#{@estimate.id}", notice: 'Estimate was successfully created.'}
        format.json { render action: 'show', status: :created, location: @estimate }
      else
        format.html { render action: 'new' }
        format.json { render json: @estimate.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @estimate.update(estimate_params)
        format.html { redirect_to "/customers/#{@estimate.customer_id}/estimates/#{@estimate.id}", notice: 'Estimate was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @estimate.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @estimate.destroy
    respond_to do |format|
      format.html { redirect_to "/customers/#{@estimate.customer_id}", notice: 'Estimate has been removed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estimate
      @estimate = Estimate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def estimate_params
      params.require(:estimate).permit(:estimate_desc_id, :customer_id, :invoice_id, :estimate_type, :estimate_address, :estimate_city, :estimate_province,
                                      :estimate_city, :estimate_details, :estimated_on_date)
    end
end