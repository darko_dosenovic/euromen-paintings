class Invoice < ActiveRecord::Base
  has_one :estimate
  belongs_to :customers

  validates_presence_of :invoice_details, :invoice_created_on, :invoice_amount, :invoice_gst, :invoice_total

end
