class Customer < ActiveRecord::Base
  has_many :estimates
  has_many :invocies

  after_initialize :set_defaults

  validates_presence_of :first_name, :last_name, :address, :city, :province, :postal_code, :home_phone

  def set_defaults
    self.city ||= 'Edmonton'
    self.province ||= 'AB'
  end
end