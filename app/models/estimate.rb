class Estimate < ActiveRecord::Base
  has_one :invoice
  belongs_to :customers

  after_initialize :set_defaults

  validates_presence_of :estimate_type, :estimate_address, :estimate_city, :estimate_province, :estimate_details, :estimate_amount,
                        :estimate_gst, :estimate_total, :estimated_on_date

  def set_defaults
    self.estimate_city ||= 'Edmonton'
    self.estimate_province ||= 'AB'
  end
end
