module CustomerHelper
  def customer_full_name(id)
    customer = Customer.find(id)
    return customer.first_name.capitalize + ' ' + customer.last_name.capitalize if customer
  end
  def customer_address(id)
    customer = Customer.find(id)
    return customer.address if customer
  end
  def customer_city_postal_code(id)
    customer = Customer.find(id)
    return customer.city.capitalize + ', ' + customer.province.capitalize + ' ' + format_postal_code(customer.postal_code) if customer
  end
  def customer_phones(id)
    customer = Customer.find(id)
    return format_phone_number(customer.home_phone) + ', ' + format_phone_number(customer.cell_phone) if customer
  end

  def format_phone_number(number)
    return "(#{number[0..2]}) #{number[3..5]}-#{number[6..-1]}"
  end
  def format_postal_code(number)
    return "#{number[0..2]} #{number[3..5]}"
  end
end
