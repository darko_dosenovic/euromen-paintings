json.array!(@invoices) do |invoice|
  json.extract! invoice, :invoice_desc_id, :po_number, :details, :created_on
  json.url invoice_url(invoice, format: :json)
end
