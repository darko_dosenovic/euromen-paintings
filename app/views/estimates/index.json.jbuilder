json.array!(@estimates) do |estimate|
  json.extract! estimate, :estimate_desc_id, :estimate.estimate_details, :estimated_on_date
  json.url estimate_url(estimate, format: :json)
end
