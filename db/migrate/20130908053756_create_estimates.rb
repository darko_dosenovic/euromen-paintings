class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :estimate_desc_id
      t.integer :customer_id
      t.integer :invoice_id
      t.string :estimate_type
      t.string :estimate_address
      t.string :estimate_city
      t.string :estimate_province
      t.string :estimate_city
      t.text :estimate_details
      t.float :estimate_amount, :default => 0
      t.boolean :estimate_gst_include
      t.float :estimate_gst, :default => 0
      t.float :estimate_total, :default => 0
      t.date :estimated_on_date

      t.timestamps
    end
  end
end
