class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :invoice_desc_id
      t.integer :customer_id
      t.integer :estimate_id
      t.string :po_number
      t.text :invoice_details
      t.float :invoice_amount, :default => 0
      t.boolean :invoice_gst_include
      t.float :invoice_gst, :default => 0
      t.float :invoice_total, :default => 0
      t.date :invoice_created_on

      t.timestamps
    end
  end
end
