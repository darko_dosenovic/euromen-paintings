class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :city
      t.string :province
      t.string :postal_code
      t.string :home_phone
      t.string :cell_phone, :null => false, :default => ""
      t.text :comments, :null => false, :default => ""
      t.timestamps
    end
  end
end
