# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ email: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#@user = User.find_or_initialize_by_email(:email => 'darko.dosenovic@ama.ab.ca', :password => 'password', :password_confirmation => 'password')
#@user.save(:validate => false)

@users = YAML.load_file("db/users.yml")
@users["users"].each do |user|
  email = user["email"]
  password = 'password'
  user = User.find_or_initialize_by_email(:email => email, :password => password)
  user.save!
end