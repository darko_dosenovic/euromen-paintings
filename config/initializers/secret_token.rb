# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
EuromenPaintings::Application.config.secret_key_base = 'ecee61f107f02cd93809952bfd32aa4c62a528aee241edf0851a619db4e17d0458c5270d7a0ca4ff93f2cd21d189b9de0734e6d17a60fc8b2e33e893564ffff6'
